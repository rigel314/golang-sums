package main

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/hex"
	"flag"
	"fmt"
	"hash"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var mode string

var GIT_VER string = "Uncontrolled"

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.Println("golang-sums version: " + GIT_VER)

	var recursive bool
	flag.BoolVar(&recursive, "r", false, "pass this flag to recurse into subdirectories of each arg")
	flag.StringVar(&mode, "c", "md5", "checksum to use, defaults to md5.  supported: md5, sha1, sha256, sha512")
	flag.Parse()

	for _, arg := range flag.Args() {
		m, err := filepath.Glob(arg)
		if err != nil {
			log.Println("bad pattern: ", arg)
			continue
		}
		if m == nil {
			m = make([]string, 1)
			m[0] = arg
		}
		for _, path := range m {
			if recursive {
				filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
					if err != nil {
						log.Println("error during recursive walk at '"+path+"': ", err)
						return nil
					}
					if !info.IsDir() {
						handleFile(path)
					}
					return nil
				})
			} else {
				handleFile(path)
			}
		}
	}
}

func handleFile(path string) {
	f, err := os.Open(path)
	if err != nil {
		log.Println("error opening file '"+path+"': ", err)
		return
	}

	var h hash.Hash
	switch strings.ToLower(mode) {
	case "md5":
		h = md5.New()
	case "sha1":
		h = sha1.New()
	case "sha256":
		h = sha256.New()
	case "sha512":
		h = sha512.New()
	}
	var buf [1024]byte
	var n int

	for err == nil {
		n, err = f.Read(buf[:])
		if n > 0 {
			h.Write(buf[:n])
		}
	}
	if err == io.EOF {
		fmt.Printf("%s  %s\n", hex.EncodeToString(h.Sum(nil)), path)
	} else {
		log.Println("error reading file '"+path+"': ", err)
	}
	f.Close()
}
